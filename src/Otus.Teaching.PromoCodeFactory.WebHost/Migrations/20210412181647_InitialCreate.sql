CREATE TABLE IF NOT EXISTS "__EFMigrationsHistory" (
    migration_id character varying(150) NOT NULL,
    product_version character varying(32) NOT NULL,
    CONSTRAINT pk___ef_migrations_history PRIMARY KEY (migration_id)
);

START TRANSACTION;


DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20210412181647_InitialCreate') THEN
CREATE TABLE customers (
                           id uuid NOT NULL,
                           first_name text NULL,
                           last_name text NULL,
                           email text NULL,
                           CONSTRAINT pk_customers PRIMARY KEY (id)
);
END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20210412181647_InitialCreate') THEN
CREATE TABLE preferences (
                             id uuid NOT NULL,
                             name text NULL,
                             CONSTRAINT pk_preferences PRIMARY KEY (id)
);
END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20210412181647_InitialCreate') THEN
CREATE TABLE roles (
                       id uuid NOT NULL,
                       name text NULL,
                       description text NULL,
                       CONSTRAINT pk_roles PRIMARY KEY (id)
);
END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20210412181647_InitialCreate') THEN
CREATE TABLE customer_preference (
                                     customer_id uuid NOT NULL,
                                     preference_id uuid NOT NULL,
                                     CONSTRAINT pk_customer_preference PRIMARY KEY (customer_id, preference_id),
                                     CONSTRAINT fk_customer_preference_customers_customer_id FOREIGN KEY (customer_id) REFERENCES customers (id) ON DELETE CASCADE,
                                     CONSTRAINT fk_customer_preference_preferences_preference_id FOREIGN KEY (preference_id) REFERENCES preferences (id) ON DELETE CASCADE
);
END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20210412181647_InitialCreate') THEN
CREATE TABLE employees (
                           id uuid NOT NULL,
                           first_name text NULL,
                           last_name text NULL,
                           email text NULL,
                           role_id uuid NOT NULL,
                           applied_promocodes_count integer NOT NULL,
                           CONSTRAINT pk_employees PRIMARY KEY (id),
                           CONSTRAINT fk_employees_roles_role_id FOREIGN KEY (role_id) REFERENCES roles (id) ON DELETE CASCADE
);
END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20210412181647_InitialCreate') THEN
CREATE TABLE promo_codes (
                             id uuid NOT NULL,
                             code text NULL,
                             service_info text NULL,
                             begin_date timestamp without time zone NOT NULL,
                             end_date timestamp without time zone NOT NULL,
                             partner_name text NULL,
                             partner_manager_id uuid NULL,
                             preference_id uuid NULL,
                             CONSTRAINT pk_promo_codes PRIMARY KEY (id),
                             CONSTRAINT fk_promo_codes_employees_partner_manager_id FOREIGN KEY (partner_manager_id) REFERENCES employees (id) ON DELETE RESTRICT,
                             CONSTRAINT fk_promo_codes_preferences_preference_id FOREIGN KEY (preference_id) REFERENCES preferences (id) ON DELETE RESTRICT
);
END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20210412181647_InitialCreate') THEN
    CREATE INDEX ix_customer_preference_preference_id ON customer_preference (preference_id);
END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20210412181647_InitialCreate') THEN
    CREATE INDEX ix_employees_role_id ON employees (role_id);
END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20210412181647_InitialCreate') THEN
    CREATE INDEX ix_promo_codes_partner_manager_id ON promo_codes (partner_manager_id);
END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20210412181647_InitialCreate') THEN
    CREATE INDEX ix_promo_codes_preference_id ON promo_codes (preference_id);
END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20210412181647_InitialCreate') THEN
    INSERT INTO "__EFMigrationsHistory" (migration_id, product_version)
    VALUES ('20210412181647_InitialCreate', '5.0.5');
END IF;
END $$;
COMMIT;