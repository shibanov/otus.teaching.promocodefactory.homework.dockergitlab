START TRANSACTION;


DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20210412182009_DataSeeding') THEN
    INSERT INTO customers (id, email, first_name, last_name)
    VALUES ('a6c8c6b1-4349-45b0-ab31-244740aaf0f0', 'ivan_sergeev@mail.ru', 'Иван', 'Петров');
END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20210412182009_DataSeeding') THEN
    INSERT INTO preferences (id, name)
    VALUES ('ef7f299f-92d7-459f-896e-078ed53ef99c', 'Театр');
INSERT INTO preferences (id, name)
VALUES ('c4bda62e-fc74-4256-a956-4760b3858cbd', 'Семья');
INSERT INTO preferences (id, name)
VALUES ('76324c47-68d2-472d-abb8-33cfa8cc0c84', 'Дети');
END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20210412182009_DataSeeding') THEN
    INSERT INTO roles (id, description, name)
    VALUES ('53729686-a368-4eeb-8bfa-cc69b6050d02', 'Администратор', 'Admin');
INSERT INTO roles (id, description, name)
VALUES ('b0ae7aac-5493-45cd-ad16-87426a5e7665', 'Партнерский менеджер', 'PartnerManager');
END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20210412182009_DataSeeding') THEN
    INSERT INTO customer_preference (customer_id, preference_id)
    VALUES ('a6c8c6b1-4349-45b0-ab31-244740aaf0f0', 'c4bda62e-fc74-4256-a956-4760b3858cbd');
INSERT INTO customer_preference (customer_id, preference_id)
VALUES ('a6c8c6b1-4349-45b0-ab31-244740aaf0f0', '76324c47-68d2-472d-abb8-33cfa8cc0c84');
END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20210412182009_DataSeeding') THEN
    INSERT INTO employees (id, applied_promocodes_count, email, first_name, last_name, role_id)
    VALUES ('451533d5-d8d5-4a11-9c7b-eb9f14e1a32f', 5, 'owner@somemail.ru', 'Иван', 'Сергеев', '53729686-a368-4eeb-8bfa-cc69b6050d02');
INSERT INTO employees (id, applied_promocodes_count, email, first_name, last_name, role_id)
VALUES ('f766e2bf-340a-46ea-bff3-f1700b435895', 10, 'andreev@somemail.ru', 'Петр', 'Андреев', 'b0ae7aac-5493-45cd-ad16-87426a5e7665');
END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "migration_id" = '20210412182009_DataSeeding') THEN
    INSERT INTO "__EFMigrationsHistory" (migration_id, product_version)
    VALUES ('20210412182009_DataSeeding', '5.0.5');
END IF;
END $$;
COMMIT;